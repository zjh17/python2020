import sqlite3
database = sqlite3.connect("王者荣耀装备数值（物理攻击）")
physical = database.cursor()

physical.execute('create table if not exists equipment(name varchar(20) primary key,money int(10),attack int(20))')

physical.execute('insert into equipment(name,money,attack) values("破军",2950,180)')
physical.execute('insert into equipment(name,money,attack) values("碎星锤",2100,100)')
physical.execute('insert into equipment(name,money,attack) values("无尽战刃",2140,130)')

physical.execute('select * from equipment')
print("原装备：",physical.fetchall())

physical.execute('update equipment set attack = 80 where name = "碎星锤"')
physical.execute('select * from equipment')
print("修改：",physical.fetchall())
physical.execute('delete from equipment where name = "无尽战刃"')
physical.execute('select * from equipment')
print("删除:",physical.fetchall())
database.close()